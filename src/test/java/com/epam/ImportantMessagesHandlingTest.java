package com.epam;

import com.epam.bo.GmailLogInBO;
import com.epam.bo.ImportantMessagesBO;
import com.epam.utils.CSVParser;
import com.epam.utils.DriverProvider;
import com.epam.utils.Property;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;

import static org.testng.Assert.assertTrue;

public class ImportantMessagesHandlingTest {
    private static final int NUMBER_OF_MESSAGE_TO_HANDLE = 2;
    private static final String DEFAULT_EMAIL = "someuser242@gmail.com";
    private static final String DEFAULT_PASSWORD = "userpass123";

    @DataProvider(parallel = true)
    private Iterator<Object[]> users() {
        try {
            return CSVParser.parse(Property.DRIVER.getProperty("credentials.path"));
        } catch (IOException e) {
            return Collections.singletonList(new Object[]{DEFAULT_EMAIL, DEFAULT_PASSWORD}).iterator();
        }
    }

    @Test(dataProvider = "users")
    private void testImportantMessageHandling(String email, String password) {
        GmailLogInBO logInBO = new GmailLogInBO();
        logInBO.login(email, password);
        assertTrue(logInBO.isSuccessfullyLoggedIn(email), "logging in Gmail failed");
        ImportantMessagesBO importantMessagesBO = new ImportantMessagesBO();
        importantMessagesBO.moveMessagesToImportant(NUMBER_OF_MESSAGE_TO_HANDLE);
        assertTrue(importantMessagesBO.areMessagesMovedToImportant(NUMBER_OF_MESSAGE_TO_HANDLE), "messages wasn't moved to important");
        importantMessagesBO.deleteMessagesFromImportant(NUMBER_OF_MESSAGE_TO_HANDLE);
        assertTrue(importantMessagesBO.areImportantMessagesRemoved(NUMBER_OF_MESSAGE_TO_HANDLE), "messages wasn't removed");
        importantMessagesBO.undoDeleting();
    }

    @AfterMethod
    private void tearDown() {
        DriverProvider.closeDriver();
    }
}
