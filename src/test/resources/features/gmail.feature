Feature: Mark messages as important and delete them
  Scenario Outline: User log in successfully
    Given User is on Gmail LogIn page
    When User log in as "<email>", "<password>"
    Then Page title contains user email "<email>"
    When User mark <messages> messages as important
    Then <messages> messages are moved to important folder
    When User deletes <messages> messages from important folder
    Then <messages> messages are deleted
    And WebDriver is closed

    Examples:
    |         email         |   password  | messages |
    | someuser242@gmail.com | userpass123 | 2        |
    | someu4163@gmail.com   | userpass123 | 2        |
    | usersome89@gmail.com  | userpass123 | 2        |
    | usersome176@gmail.com | userpass123 | 2        |
    | someuser037@gmail.com | userpass123 | 2        |
