package com.epam.element;

import com.epam.element.impl.Button;
import com.epam.element.impl.CheckBox;
import com.epam.element.impl.TextInput;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.pagefactory.DefaultFieldDecorator;
import org.openqa.selenium.support.pagefactory.ElementLocator;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;

import java.lang.reflect.*;
import java.util.List;
import java.util.Objects;

public class CustomFieldDecorator extends DefaultFieldDecorator {
    public CustomFieldDecorator(ElementLocatorFactory factory) {
        super(factory);
    }

    @Override
    public Object decorate(ClassLoader loader, Field field) {
        ElementLocator locator = factory.createLocator(field);
        if (Button.class.isAssignableFrom(field.getType())) {
            return new Button(proxyForLocator(loader, locator));
        } else if (TextInput.class.isAssignableFrom(field.getType())) {
            return new TextInput(proxyForLocator(loader, locator));
        } else if(CheckBox.class.isAssignableFrom(field.getType())){
            return new CheckBox(proxyForLocator(loader, locator));
        } else if (List.class.isAssignableFrom(field.getType())) {
            return createList(loader, locator, decoratableClass(field));
        } else if (PageElement.class.isAssignableFrom(field.getType())) {
            return new PageElement(proxyForLocator(loader, locator));
        } else {
            return super.decorate(loader, field);
        }
    }

    @SuppressWarnings("unchecked")
    private List<PageElement> createList(ClassLoader loader, ElementLocator locator,
                                         Class<PageElement> clazz) {
        InvocationHandler handler = new CustomElementListDecorator(locator, clazz);
        return (List<PageElement>) Proxy.newProxyInstance(loader,
                new Class[] {List.class}, handler);
    }

    @SuppressWarnings("unchecked")
    private Class<PageElement> decoratableClass(Field field) {
        Class<?> clazz = field.getType();
        if (List.class.isAssignableFrom(clazz)) {
            if (Objects.isNull(field.getAnnotation(FindBy.class))
                    && Objects.isNull(field.getAnnotation(FindBys.class))) {
                return null;
            }
            Type genericType = field.getGenericType();
            if (!(genericType instanceof ParameterizedType)) {
                return null;
            }
            clazz = (Class<?>) ((ParameterizedType) genericType).getActualTypeArguments()[0];
        }
        if (PageElement.class.isAssignableFrom(clazz)) {
            return (Class<PageElement>) clazz;
        } else {
            return null;
        }
    }
}
