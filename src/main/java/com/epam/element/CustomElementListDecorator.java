package com.epam.element;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.pagefactory.ElementLocator;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class CustomElementListDecorator implements InvocationHandler {
    private final ElementLocator locator;
    private final Class<PageElement> clazz;

    public CustomElementListDecorator(ElementLocator locator, Class<PageElement> clazz) {
        this.locator = locator;
        this.clazz = clazz;
    }

    public Object invoke(Object object, Method method,
                         Object[] objects) throws Throwable {
        List<WebElement> elements = locator.findElements();
        List<PageElement> checkboxes = new ArrayList<>();
        for (WebElement element : elements) {
            checkboxes.add(createInstance(clazz, element));
        }
        try {
            return method.invoke(checkboxes, objects);
        } catch (InvocationTargetException e) {
            throw e.getCause();
        }
    }

    private static PageElement createInstance(Class<PageElement> clazz, WebElement element) {
        try {
            return clazz.getConstructor(WebElement.class).newInstance(element);
        } catch (NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
            throw new AssertionError("WebElement can't be represented as " + clazz);
        }
    }
}
