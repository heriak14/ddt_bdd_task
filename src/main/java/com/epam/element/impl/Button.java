package com.epam.element.impl;

import com.epam.element.PageElement;
import com.epam.utils.Waiter;
import org.openqa.selenium.WebElement;

public class Button extends PageElement {
    public Button(WebElement element) {
        super(element);
    }

    public void waitAndClick() {
        Waiter.elementToBeClickable(element);
        element.click();
    }
}
