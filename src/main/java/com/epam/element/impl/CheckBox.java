package com.epam.element.impl;

import com.epam.element.PageElement;
import com.epam.utils.Waiter;
import org.openqa.selenium.WebElement;

public class CheckBox extends PageElement {
    public CheckBox(WebElement element) {
        super(element);
    }

    public void setChecked() {
        if (!element.isSelected()) {
            Waiter.elementToBeClickable(element);
            element.click();
        }
    }
}
