package com.epam.po.impl;

import com.epam.element.impl.Button;
import com.epam.element.impl.TextInput;
import com.epam.po.BasePage;
import com.epam.utils.DriverProvider;
import com.epam.utils.Property;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.support.FindBy;

public class EmailInputPage extends BasePage {
    private static final Logger LOGGER = LogManager.getLogger();
    @FindBy(id = "identifierId")
    private TextInput emailField;
    @FindBy(id = "identifierNext")
    private Button nextButton;

    public void enterEmail(String email) {
        LOGGER.info("entering email");
        emailField.inputText(email);
    }

    public void clickNextButton() {
        LOGGER.info("clicking 'next' button");
        nextButton.waitAndClick();
    }
}
