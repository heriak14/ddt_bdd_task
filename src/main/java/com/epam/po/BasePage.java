package com.epam.po;

import com.epam.element.CustomFieldDecorator;
import com.epam.utils.DriverProvider;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;

public class BasePage {

    public BasePage() {
        PageFactory.initElements(new CustomFieldDecorator(
                new DefaultElementLocatorFactory(DriverProvider.getDriver())), this);
    }
}
