package com.epam.bo;

import com.epam.po.impl.ImportantMessagesPage;
import com.epam.po.impl.IncomingMessagesPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ImportantMessagesBO {
    private static final Logger LOGGER = LogManager.getLogger();
    private IncomingMessagesPage incomingPage;
    private ImportantMessagesPage importantPage;
    private int startStarredMessagesNumber;
    private int endStarredMessagesNumber;

    public ImportantMessagesBO() {
        incomingPage = new IncomingMessagesPage();
        importantPage = new ImportantMessagesPage();
    }

    public void moveMessagesToImportant(int number) {
        LOGGER.info("moving" + number + "messages to important");
        incomingPage.markMessagesAsImportant(number);
        incomingPage.switchToImportantMessages();
    }

    public boolean areMessagesMovedToImportant(int number) {
        return incomingPage.areCheckBoxesSelected(number);
    }

    public void deleteMessagesFromImportant(int number) {
        LOGGER.info("removing moved messages from important");
        startStarredMessagesNumber = importantPage.getCheckBoxesNumber();
        importantPage.selectMessages(number);
        importantPage.clickDeleteButton();
        endStarredMessagesNumber = importantPage.getCheckBoxesNumber();
    }

    public boolean areImportantMessagesRemoved(int number) {
        LOGGER.info("checking if " + number + " starred messages were removed");
        return importantPage.isUndoButtonVisible()
                && (startStarredMessagesNumber - endStarredMessagesNumber) == number;
    }

    public void undoDeleting() {
        importantPage.clickUndoButton();
    }
}
