package com.epam.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class DriverProvider {
    private static final int DEFAULT_IMPLICIT_WAIT = Integer.parseInt(Property.DRIVER
            .getProperty("wait.implicit.default"));
    private static ThreadLocal<WebDriver> DRIVER_PULL = new ThreadLocal<>();

    static {
        System.setProperty(Property.DRIVER.getProperty("driver.name"), Property.DRIVER.getProperty("driver.path"));
    }

    private DriverProvider() {
    }

    public static WebDriver getDriver() {
        if (Objects.isNull(DRIVER_PULL.get())) {
            initDriver();
        }
        return DRIVER_PULL.get();
    }

    private static void initDriver() {
        DRIVER_PULL.set(new ChromeDriver());
        DRIVER_PULL.get().manage().timeouts().implicitlyWait(DEFAULT_IMPLICIT_WAIT, TimeUnit.SECONDS);
    }

    public static void closeDriver() {
        DRIVER_PULL.get().quit();
        DRIVER_PULL.set(null);
    }
}
