package com.epam.utils;

import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Waiter {
    private static final int DEFAULT_WAITING_TIME = Integer.parseInt(Property.DRIVER.getProperty("wait.explicit.default"));

    public static void titleContains(String str) {
        new WebDriverWait(DriverProvider.getDriver(), DEFAULT_WAITING_TIME).until(d -> d.getTitle().contains(str));
    }

    public static void elementToBeClickable(WebElement element) {
        new WebDriverWait(DriverProvider.getDriver(), DEFAULT_WAITING_TIME).ignoring(StaleElementReferenceException.class)
                .until(ExpectedConditions.elementToBeClickable(element));
    }

    public static void visibilityOf(WebElement element) {
        new WebDriverWait(DriverProvider.getDriver(), DEFAULT_WAITING_TIME).until(ExpectedConditions.visibilityOfAllElements(element));
    }
}
